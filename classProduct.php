<?php

	class Product
	{
		public static $companyName = 'OOO PineApple';
		const YEAR_START = '01.04.2020';

		public $name;
		public $nameFontSize;
		public $price;
		public $priceFontSize;
		public $weight;
		public $weightFontSize;
		public $image;
		public $border;
		public $bg;

		public static function infoCompany()
		{
			echo 'Наименование организации: ' . self::$companyName . '<br>';
			echo 'Дата основания: ' . Product::YEAR_START;
		}

		public function __construct(string $name, int $price, float $weight, $image, string $border, string $bg, int $nameFontSize = 20, int $priceFontSize = 16, int $weightFontSize = 16)
		{
			$this->name = $name;
			$this->nameFontSize = $nameFontSize;
			$this->price = $price;
			$this->priceFontSize = $priceFontSize;
			$this->weight = $weight;
			$this->weightFontSize = $weightFontSize;
			$this->image = $image;
			$this->border = $border;
			$this->bg = $bg;
		}

		public function printProduct()
		{
			echo "<div>
			<h2 style='font-size: {$this->nameFontSize}px'> {$this->name} </h2>
			<span>	Цена: {$this->price} руб. <br> Вес: {$this->weight} кг. </span>
			</div>";
		}

		public function showImage()
		{
			echo "<div> <img src='{$this->image}' alt='Картинка' width='200'>
			</div>";
		}
	}

?>