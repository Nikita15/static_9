<?php

	class PrintPrice extends Product
	{
		public $priceNotNds;
		public $priceNotNdsFontSize;

		public function __construct(string $name, int $price, int $priceNotNds, float $weight, $image, string $border, string $bg, int $nameFontSize = 20, int $priceFontSize = 16, int $priceNotNdsFontSize = 16, int $weightFontSize = 16)
		{
			$this->priceNotNds = $priceNotNds;
			$this->priceNotNdsFontSize = $priceNotNdsFontSize;
		
			parent::__construct($name, $price, $weight, $image, $border, $bg, $nameFontSize, $priceFontSize, $weightFontSize);
		}

		public function printProduct()
		{
			echo "<div style='border: {$this->border}; background: {$this->bg};'>
			<h2 style='font-size: {$this->nameFontSize}px'>{$this->name} </h2>
			<span> Стоимость товара: {$this->price} руб. <br> Стоимость товара без учёта НДС: {$this->priceNotNds} руб. </span>
			</div>";
		}

	}

?>