<?php

	class Chocolate extends Product
	{
		public $kkal;

		public function __construct(string $name, int $price, float $weight, float $kkal, $image, string $border, string $bg, int $nameFontSize = 20, int $priceFontSize = 16, int $weightFontSize = 16)
		{
			$this->kkal = $kkal;
		
			parent::__construct($name, $price, $weight, $image, $border, $bg, $nameFontSize, $priceFontSize, $weightFontSize);
		}

		public function printInfProduct()
		{
			echo "<div style='border: {$this->border}; background: {$this->bg};'>
			<h2 style='font-size: {$this->nameFontSize}px'>{$this->name} </h2>
			<span>	Цена: {$this->price} руб. <br> Вес: {$this->weight} кг. <br> Ккал: {$this->kkal} </span>
			</div>";
		}
	}
//<h1> {$this->companyName} </h1>
?>